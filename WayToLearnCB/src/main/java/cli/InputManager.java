package cli;

import java.util.Scanner;

public class InputManager {

    private static final Scanner sc = new Scanner(System.in);

    private static final InputManager INSTANCE = new InputManager();

    public static InputManager getInstance() { return INSTANCE;}

    private InputManager(){
    }

    public int getIntStudentChoice(){
        Integer choice = null;

        do{
            try{
                choice = Integer.parseInt(sc.nextLine());
            } catch (Exception e) {
                System.out.println("Entrée incorrecte - Veuillez réessayer");
            }
        }while (choice == null);

        return choice;
    }

    public String getStringStudentChoice(){
        String choice = null;

        do{
            try{
                choice = sc.nextLine();
            } catch (Exception e) {
                System.out.println("Entrée incorrecte - Veuillez réessayer");
            }
        }while (choice == null);

        return choice;
    }
}
