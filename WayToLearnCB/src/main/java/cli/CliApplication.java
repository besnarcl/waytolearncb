package cli;

import cli.Screen.MenuScreen;
import cli.Screen.PooScreen;
import domain.Score;

import java.util.Scanner;

public class CliApplication {


    public static void main(String[] args){
        Scanner sc = new Scanner(System.in);
        MenuScreen menuScreen = new MenuScreen();
        PooScreen pooScreen = new PooScreen();

        menuScreen.display(sc);


    }
}
