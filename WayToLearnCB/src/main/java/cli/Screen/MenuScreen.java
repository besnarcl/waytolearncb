package cli.Screen;

import cli.InputManager;

import java.util.Scanner;

public class MenuScreen implements Screen {
    public void display(Scanner sc) {
        int choice;
        do {
        System.out.println("""
                === QCM WayToLearnX ===

                Quel QCM souhaitez vous réalisez ?
                   1 - Java - Programmation orientée objet
                   2 - Java - Les collections - partie 1
                   3 - Quitter l’application

                Votre choix : <saisie de l’utilisateur>
                """);

        choice = InputManager.getInstance().getIntStudentChoice();


           switch (choice) {
               case 1:
                   new PooScreen().display(sc);
                   break;
               case 2:
                   new CollectionsScreen().display(sc);
                   break;
               case 3:
                   new QuitScreen().display(sc);
                   break;
               default:
                   System.out.println("Saisie incorrecte");
           }
       }while(false);
    }
}
