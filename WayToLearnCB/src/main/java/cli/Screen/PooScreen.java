package cli.Screen;


import cli.InputManager;
import domain.Answer;
import domain.QCM;
import domain.Question;
import domain.Score;

import java.util.List;
import java.util.Scanner;

public class PooScreen implements Screen {
    private Score score = new Score(0);
    private ScoreScreen scoreScreen = new ScoreScreen(score);

    // QCM Poo Réponses
// réponse à la question 1
    Answer answer1A = new Answer("A", "Héritage");
    Answer answer1B = new Answer("B", "Encapsulation");
    Answer answer1C = new Answer("C", "Polymorphisme");
    Answer answer1D = new Answer("D", "Compilation");

    List<Answer> answers1 = List.of(answer1A, answer1B, answer1C, answer1D);
    List<Answer> goodAnswer1 = List.of(answer1D);

    // réponse à la question 2
    Answer answer2A = new Answer("A", "Au moment de l’exécution");
    Answer answer2B = new Answer("B", "Au moment de la compilation");
    Answer answer2C = new Answer("C", "Au moment du codage");
    Answer answer2D = new Answer("D", "Au moment de l’exécution");

    List<Answer> answers2 = List.of(answer2A, answer2B, answer2C, answer2D);
    List<Answer> goodAnswer2 = List.of(answer2B);

    // réponse à la question 3
    Answer answer3A = new Answer("A", "Quand il y a plusieurs méthodes avec le même nom mais avec une signature de méthode différente et un nombre ou un type de paramètres différent");
    Answer answer3B = new Answer("B", "Quand il y a plusieurs méthodes avec le même nom, le même nombre de paramètres et le type mais une signature différente");
    Answer answer3C = new Answer("C", "Quand il y a plusieurs méthodes avec le même nom, la même signature, le même nombre de paramètres mais un type différent");
    Answer answer3D = new Answer("D", "Quand il y a plusieurs méthodes avec le même nom, la même signature mais avec différente signature");

    List<Answer> answers3 = List.of(answer3A, answer3B, answer3C, answer3D);
    List<Answer> goodAnswer3 = List.of(answer3B);

    // réponse à la question 4
    Answer answer4A = new Answer("A", "Polymorphisme");
    Answer answer4B = new Answer("B", "Encapsulation");
    Answer answer4C = new Answer("C", "Abstraction");
    Answer answer4D = new Answer("D", "Héritage");

    List<Answer> answers4 = List.of(answer4A, answer4B, answer4C, answer4D);
    List<Answer> goodAnswer4 = List.of(answer4C);

    // réponse à la question 5
    Answer answer5A = new Answer("A", "Polymorphisme");
    Answer answer5B = new Answer("B", "Encapsulation");
    Answer answer5C = new Answer("C", "Abstraction");
    Answer answer5D = new Answer("D", "Héritage");

    List<Answer> answers5 = List.of(answer5A, answer5B, answer5C, answer5D);
    List<Answer> goodAnswer5 = List.of(answer5B);

    // réponse à la question 6
    Answer answer6A = new Answer("A", "Agrégation");
    Answer answer6B = new Answer("B", "Composition");
    Answer answer6C = new Answer("C", "Encapsulation");
    Answer answer6D = new Answer("D", "Association");

    List<Answer> answers6 = List.of(answer6A, answer6B, answer6C, answer6D);
    List<Answer> goodAnswer6 = List.of(answer6D);

    // réponse à la question 7
    Answer answer7A = new Answer("A", "Agrégation");
    Answer answer7B = new Answer("B", "Composition");
    Answer answer7C = new Answer("C", "Encapsulation");
    Answer answer7D = new Answer("D", "Association");

    List<Answer> answers7 = List.of(answer7A, answer7B, answer7C, answer7D);
    List<Answer> goodAnswer7 = List.of(answer7B);

    // réponse à la question 8
    Answer answer8A = new Answer("A", "Agrégation");
    Answer answer8B = new Answer("B", "Composition");
    Answer answer8C = new Answer("C", "Encapsulation");
    Answer answer8D = new Answer("D", "Association");

    List<Answer> answers8 = List.of(answer8A, answer8B, answer8C, answer8D);
    List<Answer> goodAnswer8 = List.of(answer8A);

    // réponse à la question 9
    Answer answer9A = new Answer("A", "Vrai");
    Answer answer9B = new Answer("B", "Faux");

    List<Answer> answers9 = List.of(answer9A, answer9B);
    List<Answer> goodAnswer9 = List.of(answer9A);

    // réponse à la question 10
    Answer answer10A = new Answer("A", "final");
    Answer answer10B = new Answer("B", "private");
    Answer answer10C = new Answer("C", "abstract");
    Answer answer10D = new Answer("D", "protected");
    Answer answer10E = new Answer("E", "public");

    List<Answer> answers10 = List.of(answer10A, answer10B, answer10C, answer10D, answer10E);
    List<Answer> goodAnswer10 = List.of(answer10B, answer10D, answer10E);

    //QCM Poo Questions
    Question question1Poo = new Question(1, "Lequel des éléments suivants n’est pas un concept POO en Java ?", answers1, goodAnswer1);
    Question question2Poo = new Question(2, "Quand la surcharge de méthode est-elle déterminée ?", answers2, goodAnswer2);
    Question question3Poo = new Question(3, "Quand la surcharge ne se produit pas ?", answers3, goodAnswer3);
    Question question4Poo = new Question(4, "Quel concept de Java est un moyen de convertir des objets du monde réel en termes de classe ?", answers4, goodAnswer4);
    Question question5Poo = new Question(5, "Quel concept de Java est utilisé en combinant des méthodes et des attributs dans une classe ?", answers5, goodAnswer5);
    Question question6Poo = new Question(6, "Comment ça s’appelle si un objet a son propre cycle de vie ?", answers6, goodAnswer6);
    Question question7Poo = new Question(7, "Comment s’appelle-t-on dans le cas où l’objet d’une classe mère est détruit donc l’objet d’une classe fille sera détruit également ?", answers7, goodAnswer7);
    Question question8Poo = new Question(8, "Comment s’appelle-t-on l’objet a son propre cycle de vie et l’objet d’une classe fille ne dépend pas à un autre objet d’une classe mère ?", answers8, goodAnswer8);
    Question question9Poo = new Question(9, "La surcharge d’une méthode peut remplacer l’héritage et le polymorphisme ?", answers9, goodAnswer9);
    Question question10Poo = new Question(10, "Quels keywords sont utilisés pour spécifier la visibilité des propriétés et des méthodes ?", answers10, goodAnswer10);

    List<Question> questionPoo = List.of(
            question1Poo,
            question2Poo,
            question3Poo,
            question4Poo,
            question5Poo,
            question6Poo,
            question7Poo,
            question8Poo,
            question9Poo,
            question10Poo
    );

    public void display(Scanner sc) {
        System.out.println("== QCM Java - Programmation orientée objet ==" + '\n');

        QCM qcmPoo = new QCM("Poo", questionPoo);
        for (int i = 0; i < questionPoo.size(); i++) {
            System.out.println();
            System.out.println("= Question " + questionPoo.get(i).getId() + '\n' + questionPoo.get(i).getFormulation() + '\n');
            for (Answer answer : questionPoo.get(i).getAnswers()) {
                System.out.println(answer.toString());
            }

            System.out.println('\n' + "Votre réponse : ");
            String answerStudent = InputManager.getInstance().getStringStudentChoice();

            for (Answer goodAnswer : questionPoo.get(i).getGoodAnswer()) {

                if (answerStudent.equals(goodAnswer.getId())) {
                    score.addScore();
                } else {
                    score.getScore();
                }
            }
        }
        scoreScreen.display(sc);

    }
}

