package cli.Screen;

import java.util.Scanner;

public interface Screen {
    public void display(Scanner sc);
}
