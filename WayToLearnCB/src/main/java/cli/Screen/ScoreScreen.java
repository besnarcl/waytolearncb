package cli.Screen;

import cli.InputManager;
import domain.QCM;
import domain.Score;

import java.util.Scanner;

public class ScoreScreen implements Screen {
    private Score score;
    private final PooCorrectionScreen pooCorrectionScreen = new PooCorrectionScreen();
    private String answer;
    MenuScreen menuScreen = new MenuScreen();

    public ScoreScreen(Score score) {
        this.score = score;
    }


    public void display(Scanner sc) {
        System.out.println(" = Résultat du QCM " + '\n');
        System.out.println("Votre score est de " + score.getScore() + "/10." + '\n');


        do {
            System.out.println("Voulez-vous voir les réponses des questions où vous avez échouées (O/N) ?");

            String answer = InputManager.getInstance().getStringStudentChoice();
            if (answer.equals("O") || answer.equals("o")) {
                pooCorrectionScreen.display(sc);

            } else if (answer.equals("N") || answer.equals("n")) {
                menuScreen.display(sc);
            } else {
                System.out.println(" Saisie incorrecte - Veuillez recommencer ");

            }
        }while(!answer.equals("O") && !answer.equals("o") && !answer.equals("N") && !answer.equals("n"));
    }
}
