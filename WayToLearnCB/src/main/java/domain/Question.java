package domain;

import java.util.List;

public class Question {
    //une question propose une liste de réponses
    // une question a un numéro, un libellé et une liste de question associée

    private int id;
    private String formulation;
    private List<Answer> answers;
    private List<Answer> goodAnswer;

    public Question(int id, String formulation, List<Answer> answers, List<Answer> goodAnswer) {
        this.id = id;
        this.formulation = formulation;
        this.answers = answers;
        this.goodAnswer = goodAnswer;
    }

    public void render(){
        System.out.println("= Question " + id + '\n' + formulation + '\n');
        for (Answer answer : answers){
            System.out.println(answer.toString());
        }

    }


    public int getId() {
        return id;
    }

    public String getFormulation() {
        return formulation;
    }

    public List<Answer> getAnswers() {
        return answers;
    }

    public List<Answer> getGoodAnswer() {
        return goodAnswer;
    }
}
