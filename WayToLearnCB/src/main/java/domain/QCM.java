package domain;

import java.util.List;

public class QCM {
    //QCM liste de questions avec choix de réponses
    private String id;
    private List<Question> questions;


    public QCM(String id, List<Question> questions) {
        this.id = id;
        this.questions = questions;
    }

    public String getId() {
        return id;
    }

    public List<Question> getQuestions() {
        return questions;
    }
}

