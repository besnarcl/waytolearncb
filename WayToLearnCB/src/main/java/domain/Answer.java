package domain;

public class Answer {
    private String id;
    private String formulation;

    public Answer(String id, String formulation) {
        this.id = id;
        this.formulation = formulation;
    }

    public String getId() {
        return id;
    }

    public String getFormulation() {
        return formulation;
    }

    public void setFormulation(String formulation) {
        this.formulation = formulation;
    }

    @Override
    public String toString() {
        return id + " - " + formulation;
    }
}
