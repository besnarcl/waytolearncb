package domain;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class QCMTest {
    Answer answer1A = new Answer("A", "Héritage");
    Answer answer1B = new Answer("B", "Encapsulation");
    Answer answer1C = new Answer("C", "Polymorphisme");
    Answer answer1D = new Answer("D", "Compilation");

    List<Answer> goodAnswer1 = List.of(answer1D);

    List<Answer> answersStudent = List.of(answer1B, answer1C);

    Score score = new Score(0);
    @Test
    void goodAnswer() {
        if(goodAnswer1.size() == answersStudent.size() && answersStudent.containsAll(goodAnswer1)){
            score.addScore();

            assertEquals(1,score.getScore());

        }else{

            assertEquals(0, score.getScore());
        }

    }
}